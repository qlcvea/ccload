# ccload

Very basic tool to upload subtitles to YouTube with some extra settings (maybe).

> Now featuring: Type out JSON to submit yourself because I did not make a form for it!

I'm pretty sure the only thing this can do that cannot be replicated in YT Studio is setting a name for a caption track.  
Even though [the documentation](https://developers.google.com/youtube/v3/docs/captions#resource) contains some other fields that look quite interesting, they do not appear to be settable via API, which is what I was trying to do with this project.

You will need to provide a Client ID in `values.json`, see `values.example.json` for a template.

## CLI Commands
*   `npm install`: Installs dependencies

*   `npm run dev`: Run a development, HMR server

*   `npm run serve`: Run a production-like server

*   `npm run build`: Production-ready build

*   `npm run lint`: Pass TypeScript files using ESLint

*   `npm run test`: Run Jest and Enzyme with
    [`enzyme-adapter-preact-pure`](https://github.com/preactjs/enzyme-adapter-preact-pure) for
    your tests


For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
