import { AxiosInstance } from "axios";

type ThumbnailName = 'default' | 'medium' | 'high' | 'standard' | 'maxres';
type CachedVideo = {
  title: string,
  description: string,
  thumbnails: {
    // see https://developers.google.com/youtube/v3/docs/videos#resource
    [name in ThumbnailName]: {
      url: string
    }
  }
  hasCaptions: boolean
};

// straight response from google, "any" == value of "snippet" from https://developers.google.com/youtube/v3/docs/captions#resource-representation
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type CachedCaption = any;

const cache = {
  videos: {} as {
    [id: string]: CachedVideo
  },
  captions: {} as {
    [videoId: string]: {
      [captionId: string]: CachedCaption 
    }
  }
};

function parseVideo(apiVideo: any): CachedVideo {
  return {
    title: apiVideo.snippet.title,
    description: apiVideo.snippet.description,
    thumbnails: apiVideo.snippet.thumbnails,
    hasCaptions: apiVideo.contentDetails.caption === 'true'
  };
}

function parseCaption(apiCaption: any): CachedCaption {
  return apiCaption.snippet;
}

async function getVideo(axios: AxiosInstance, id: string, forceGet = false): Promise<CachedVideo> {
  if (forceGet || typeof cache.videos[id] !== 'object') {
    const url = new URL('https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails');
    url.searchParams.set('id', id);
    const res = await axios({ url: url.toString() });
    cache.videos[id] = parseVideo(res.data.items[0]);
    if (!cache.videos[id].hasCaptions) cache.captions[id] = {};
  }

  return cache.videos[id];
}

async function getCaptions(axios: AxiosInstance, videoId: string, forceGet = false): Promise<{ [captionId: string]: CachedCaption }> {
  if (forceGet || typeof cache.captions[videoId] !== 'object') {
    const url = new URL('https://www.googleapis.com/youtube/v3/captions?part=id,snippet');
    url.searchParams.set('videoId', videoId);
    const res = await axios({ url: url.toString() });
    cache.captions[videoId] = {};
    for (const caption of res.data.items) {
      cache.captions[videoId][caption.id] = parseCaption(caption);
    }
  }

  return cache.captions[videoId];
}

async function getCaption(axios: AxiosInstance, videoId: string, captionId: string, forceGet = false): Promise<CachedCaption | null> {
  const captions = await getCaptions(axios, videoId, forceGet);
  if (typeof captions[captionId] === 'object') {
    return captions[captionId];
  }
  return null;
}

export { cache, getVideo, getCaptions, getCaption };
export type { CachedVideo, CachedCaption };