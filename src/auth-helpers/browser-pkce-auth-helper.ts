import axios from 'axios';
import { AuthenticationStateKnown } from 'src/components/auth';
import { cryptoRandomString } from '../util';
import AuthHelper from './auth-helper';

// Leaving this here, even though it is incomplete.
// I got to this point before realizing that for whatever reason Google does not implement PKCE for web apps

/**
 * @deprecated Google does not implement PKCE for web apps, therefore this helper does not work
 */
export default class BrowserPKCEAuthHelper extends AuthHelper {
  private _storageKey = 'auth';
  private _getStateStorageKey: (state: string) => string = (state) => `state.${state}`;

  private _clientId: string;
  private _redirectUri: string;

  constructor(clientId: string, redirectUri = `${window.location.origin}/callback`) {
    super();
    this._clientId = clientId;
    this._redirectUri = redirectUri;
  }

  beginSignIn = async () => {
    const codeVerifier = cryptoRandomString(64);
    const state = cryptoRandomString(64);
    window.sessionStorage.setItem(this._getStateStorageKey(state), JSON.stringify({ v: codeVerifier, h: window.location.href }));
    if (!crypto.subtle) throw new Error('SubtleCrypto is unavailable');

    const digest = await crypto.subtle.digest('SHA-256', new TextEncoder().encode(codeVerifier));
    const codeChallenge = btoa(String.fromCharCode(...new Uint8Array(digest))).replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_')

    const signInUri = new URL('https://accounts.google.com/o/oauth2/v2/auth');
    signInUri.searchParams.set('client_id', this._clientId);
    signInUri.searchParams.set('scope', 'https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtube');
    signInUri.searchParams.set('redirect_uri', this._redirectUri);
    signInUri.searchParams.set('response_type', 'code');
    signInUri.searchParams.set('prompt', 'select_account');
    signInUri.searchParams.set('state', state);
    signInUri.searchParams.set('code_challenge_method', 'S256');
    signInUri.searchParams.set('code_challenge', codeChallenge);
    window.location.href = signInUri.toString();
  }

  signOut = async () => {
    window.localStorage.removeItem(this._storageKey);
  }

  getAuth = async (): Promise<AuthenticationStateKnown> => {
    if (window.location.origin + window.location.pathname === this._redirectUri) {
      // Check for returned values and get token if they are present
      const currentUrl = new URL(window.location.href);
      const state = currentUrl.searchParams.get('state');
      if (state !== null) {
        const storedStateStr = window.sessionStorage.getItem(this._getStateStorageKey(state));

        // "Consume" state by deleting it
        window.sessionStorage.removeItem(this._getStateStorageKey(state));

        if (storedStateStr === null) return { signedIn: false };
        const storedState = (() => {
          try {
            return JSON.parse(storedStateStr);
          } catch (err) {
            console.error('Failed to parse stored state:', err);
            return null;
          }
        })();
    
        // Check that storedState matches schema
        // { "v": "codeVerifier", "h": "url to redirect to after auth" }
        if (
          typeof storedState !== 'object' || storedState === null ||
          typeof storedState.v !== 'string' || typeof storedState.h !== 'string'
        ) return { signedIn: false };

        // Whatever happens, redirect to original URL from state
        // To do this, the entire thing is wrapped in a function that is called
        let returnValue: AuthenticationStateKnown | undefined;
        let errorValue;
        try {
          returnValue = await (async () => {
            const code = currentUrl.searchParams.get('code');
            if (code !== null) {
              const res = await (async () => {
                try {
                  return await axios({
                    url: 'https://oauth2.googleapis.com/token',
                    method: 'POST',
                    data: {
                      code,
                      client_id: this._clientId,
                      client_secret: storedState.v,
                      grant_type: 'authorization_code',
                      redirect_uri: this._redirectUri
                    }
                  });
                } catch (err) {
                  console.error('Code exchange failed:', err);
                  return null;
                }
              })();
              if (res === null) return { signedIn: false };
              if (res.status !== 200) {
                console.error('Code exchange failed');
                return { signedIn: false };
              }
              console.log(res.data);
            } else {
              return { signedIn: false };
            }
          })();
        } catch (err) {
          errorValue = err;
        }

        window.location.href = storedState.h;

        if (typeof returnValue === 'undefined') throw errorValue;
        else return returnValue;
      }
    }

    const storedAuthStr = window.localStorage.getItem(this._storageKey);
    if (storedAuthStr === null) return { signedIn: false };
    const storedAuth = (() => {
      try {
        return JSON.parse(storedAuthStr);
      } catch (err) {
        console.error('Failed to parse stored authentication:', err);
        return null;
      }
    })();

    // Check that storedAuth matches schema
    // { "t": "token", "e": 000(unix time), "r": "refresh token, optional" }
    if (
      typeof storedAuth !== 'object' || storedAuth === null ||
      typeof storedAuth.t !== 'string' ||
      typeof storedAuth.e !== 'number'
    ) return { signedIn: false };

    if (Math.floor(new Date().getTime() / 1000) >= storedAuth.e) {
      // Token expired
      // TODO: attempt refresh
      return { signedIn: false };
    }
    return {
      signedIn: true,
      token: storedAuth.t,
      tokenExpires: new Date(storedAuth.e * 1000)
    };
  }
}