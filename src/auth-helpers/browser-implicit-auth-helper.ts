import { AuthenticationStateKnown } from 'src/components/auth';
import { cryptoRandomString } from '../util';
import AuthHelper from './auth-helper';

export default class BrowserImplicitAuthHelper extends AuthHelper {
  private _storageKey = 'auth';
  private _getStateStorageKey: (state: string) => string = (state) => `state.${state}`;

  private _clientId: string;
  private _redirectUri: string;

  constructor(clientId: string, redirectUri = `${window.location.origin}/callback`) {
    super();
    this._clientId = clientId;
    this._redirectUri = redirectUri;
  }

  beginSignIn = async () => {
    const state = cryptoRandomString(64);
    window.sessionStorage.setItem(this._getStateStorageKey(state), JSON.stringify({ h: window.location.href }));

    const signInUri = new URL('https://accounts.google.com/o/oauth2/v2/auth');
    signInUri.searchParams.set('client_id', this._clientId);
    signInUri.searchParams.set('scope', 'https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/youtube');
    signInUri.searchParams.set('redirect_uri', this._redirectUri);
    signInUri.searchParams.set('response_type', 'token');
    signInUri.searchParams.set('prompt', 'select_account');
    signInUri.searchParams.set('state', state);
    window.location.href = signInUri.toString();
  }

  signOut = async () => {
    window.localStorage.removeItem(this._storageKey);
  }

  getAuth = async (): Promise<AuthenticationStateKnown> => {
    if (window.location.origin + window.location.pathname === this._redirectUri) {
      // Check for returned values and get token if they are present
      const hashParams = (() => {
        try {
          if (window.location.hash.length < 2) throw new Error('hash too short');
          return new URLSearchParams(window.location.hash.substring(1));
        } catch (err) {
          console.error('Failed to parse parameters:', err);
          return null;
        }
      })();
      if (hashParams === null) return { signedIn: false };

      const state = hashParams.get('state');
      if (state !== null) {
        const storedStateStr = window.sessionStorage.getItem(this._getStateStorageKey(state));

        // "Consume" state by deleting it
        window.sessionStorage.removeItem(this._getStateStorageKey(state));

        if (storedStateStr === null) return { signedIn: false };
        const storedState = (() => {
          try {
            return JSON.parse(storedStateStr);
          } catch (err) {
            console.error('Failed to parse stored state:', err);
            return null;
          }
        })();
    
        // Check that storedState matches schema
        // { "v": "codeVerifier", "h": "url to redirect to after auth" }
        if (
          typeof storedState !== 'object' || storedState === null ||
          typeof storedState.h !== 'string'
        ) return { signedIn: false };

        // Whatever happens, redirect to original URL from state
        // To do this, the entire thing is wrapped in a function that is called
        let returnValue: AuthenticationStateKnown | undefined;
        let errorValue;
        try {
          returnValue = await (async () => {
            console.log(hashParams);
            const token = hashParams.get('access_token');
            const expiresInStr = hashParams.get('expires_in');
            if (token !== null && expiresInStr !== null) {
              const expiresIn = parseInt(expiresInStr, 10);
              if (!isNaN(expiresIn)) {
                const expires = Math.floor(new Date().getTime() / 1000) + expiresIn;
                window.localStorage.setItem(this._storageKey, JSON.stringify({ t: token, e: expires }))
                return { signedIn: true, token, tokenExpires: new Date(expires * 1000) };
              }
            }
            return { signedIn: false };
          })();
        } catch (err) {
          errorValue = err;
        }

        window.location.href = storedState.h;

        if (typeof returnValue === 'undefined') throw errorValue;
        else return returnValue;
      }
    }

    const storedAuthStr = window.localStorage.getItem(this._storageKey);
    if (storedAuthStr === null) return { signedIn: false };
    const storedAuth = (() => {
      try {
        return JSON.parse(storedAuthStr);
      } catch (err) {
        console.error('Failed to parse stored authentication:', err);
        return null;
      }
    })();

    // Check that storedAuth matches schema
    // { "t": "token", "e": 000(unix time), "r": "refresh token, optional" }
    if (
      typeof storedAuth !== 'object' || storedAuth === null ||
      typeof storedAuth.t !== 'string' ||
      typeof storedAuth.e !== 'number'
    ) return { signedIn: false };

    if (Math.floor(new Date().getTime() / 1000) >= storedAuth.e) {
      // Token expired
      return { signedIn: false };
    }
    return {
      signedIn: true,
      token: storedAuth.t,
      tokenExpires: new Date(storedAuth.e * 1000)
    };
  }
}