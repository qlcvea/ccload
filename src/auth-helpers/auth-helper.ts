import { AuthenticationStateKnown } from '../components/auth';

export default abstract class AuthHelper {
  abstract beginSignIn(): void;
  abstract signOut(): Promise<void>;
  abstract getAuth(): Promise<AuthenticationStateKnown>;
}