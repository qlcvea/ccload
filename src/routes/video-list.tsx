import { FunctionalComponent, h } from 'preact';
import { Link, route } from 'preact-router';
import { useContext, useState } from 'preact/hooks';
import Pagination from '../components/pagination';
import { authContext } from '../components/auth';
import Loading from '../components/loading';

const VideoList: FunctionalComponent<unknown> = () => {
  const auth = useContext(authContext);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [ list, setList ] = useState<any>(undefined);
  const searchParams = new URLSearchParams(window.location.search);

  if (typeof list === 'undefined') {
    // TODO: begin loading
    if (auth.signedIn) {
      setList(null);
      (async () => {
        const url = new URL('https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&forMine=true&safeSearch=none&order=date');
        const pageToken = searchParams.get('p');
        if (pageToken !== null) {
          url.searchParams.set('pageToken', pageToken);
        }
        const res = await auth.axios({ url: url.toString() });
        setList(res.data);
      })();
    }
  }

  if (typeof list === 'undefined' || list === null)
    return (
      <Loading />
    );
  
  const onNext = typeof list.nextPageToken === 'string' ? () => {
    searchParams.set('p', list.nextPageToken)
    route(`${window.location.pathname}?${searchParams.toString()}`, true);
    setList(undefined);
  } : null;
  const onPrevious = typeof list.prevPageToken === 'string' ? () => {
    searchParams.set('p', list.prevPageToken)
    route(`${window.location.pathname}?${searchParams.toString()}`, true);
    setList(undefined);
  } : null;
  
  return (
    <div className="m">
      <Pagination onNext={onNext} onPrevious={onPrevious} />
      <table>
        <tbody>
          { // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (list as { items: any[] }).items.map((item, index) => (
            <tr key={index}>
              <td>
                <Link href={`/videos/${item.id.videoId}`}><img src={item.snippet. thumbnails.default.url} alt={item.snippet.title} /></Link>
              </td>
              <td><Link href={`/videos/${item.id.videoId}`}>{item.snippet.title}</Link></td>
            </tr>
          ))}
        </tbody>
      </table>
      <Pagination onNext={onNext} onPrevious={onPrevious} />
    </div>
  )
};

export default VideoList;
