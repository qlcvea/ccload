import { Component } from 'preact';
import { route } from 'preact-router';

export default class Home extends Component {
  componentWillMount() {
    route('/videos', true);
  }

  render() {
    return null;
  }
}
