import { FunctionalComponent, Fragment, h } from 'preact';
import { useContext, useState } from 'preact/hooks';
import { authContext } from '../components/auth';
import Loading from '../components/loading';
import { cache, CachedCaption, getCaption } from '../cache';
import NotFound from './not-found';
import { route } from 'preact-router';

const Caption: FunctionalComponent<{ videoId: string, captionId: string | undefined }> = ({ videoId, captionId }) => {
  const auth = useContext(authContext);
  const [ caption, setCaption ] = useState<{ [captionId: string]: CachedCaption } | undefined | null | false>(undefined);
  const [ textareaContent, setTextareaContent ] = useState<string>('');

  if (typeof caption === 'undefined') {
    if (auth.signedIn) {
      if (typeof captionId === 'undefined') {
        // New track
        setCaption(false);
        setTextareaContent(JSON.stringify({
          // Default values
          language: 'xx',
          name: 'xx'
        }, undefined, 2));
      } else {
        // Existing track
        setCaption(null);
        (async () => {
          const caption = await getCaption(auth.axios, videoId, captionId);
          setCaption(caption === null ? false : caption);
          setTextareaContent(JSON.stringify(caption, undefined, 2));
        })();
      }
    }
  }

  if (typeof caption === 'undefined' || caption === null)
    return (
      <Loading />
    );
  else if (caption === false && typeof captionId !== 'undefined')
    return (
      <NotFound />
    );

  return (
    <div className="m">
      <h2>{caption === false ? 'New caption track' : `${caption.language}${(typeof caption.name === 'string' && caption.name.length >= 1 ? ` - ${caption.name}` : '')}` }</h2>
      <textarea onChange={(e) => {
        setTextareaContent((e.target as HTMLTextAreaElement).value);
      }}>{textareaContent}</textarea>
      <div className="p">
        <button onClick={async () => {
          route(`/videos/${videoId}/captions`, false);
        }}>Cancel</button>
        <button onClick={async () => {
          if (typeof captionId === 'undefined') {
            // New track

            // Select file
            const input = window.document.createElement('input');
            input.type = 'file';
            input.multiple = false;
            input.click();
            input.addEventListener('change', async () => {
              if (input.files?.length === 1) {
                setCaption(null);
                const snippet = JSON.parse(textareaContent);
                snippet.isDraft = true;
                snippet.videoId = videoId;
                const data = new FormData();
                data.append('caption', new Blob([ JSON.stringify({ snippet }) ], { type: 'application/json' }))
                data.append('file', new Blob([ input.files[0] ], { type: input.files[0].type }))
                const res = await auth.axios({
                  url: `https://www.googleapis.com/upload/youtube/v3/captions?part=id,snippet&sync=false&uploadType=multipart`,
                  method: 'POST',
                  headers: {
                    'Content-Type': 'multipart/related'
                  },
                  data
                });

                if (typeof cache.captions[videoId] !== 'object') cache.captions[videoId] = {};
                cache.captions[videoId][res.data.id] = res.data.snippet;
                if (typeof cache.videos[videoId] === 'object') cache.videos[videoId].hasCaptions = true;
                route(`/videos/${videoId}/captions/${res.data.id}`, true);
                setCaption(await getCaption(auth.axios, videoId, res.data.id));
                setTextareaContent(JSON.stringify(caption, undefined, 2));
              }
            });
          } else {
            // Existing track
            setCaption(null);
            const res = await auth.axios({
              url: `https://www.googleapis.com/youtube/v3/captions?part=id,snippet`,
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json'
              },
              data: JSON.stringify({
                id: captionId,
                snippet: JSON.parse(textareaContent)
              })
            });
            cache.captions[videoId][res.data.id] = res.data.snippet;
            setCaption(await getCaption(auth.axios, videoId, captionId));
            setTextareaContent(JSON.stringify(caption, undefined, 2));
          }
        }}
          disabled={typeof captionId !== 'undefined' /* Unfortunately it does not appear that settings other than isDraft can be updated */}
        >{typeof captionId === 'undefined' ? 'Choose file & save' : 'Save'}</button>
        {/*
        // does not work because for whatever reason it does not have cors set
        <button onClick={async () => {
          setCaption(null);
          const res = await auth.axios({ url: `https://www.googleapis.com/youtube/v3/captions/${encodeURIComponent(captionId)}` });
          const blob = new Blob([ res.data ], { type: 'application/octet-stream' });
          const a = document.createElement('a');
          a.href = URL.createObjectURL(blob);
          a.download = 'captions';
          a.click();
          URL.revokeObjectURL(a.href);
          setCaption(caption);
        }}>Download</button>*/}
        {
          typeof captionId !== 'undefined' && (<>
            <button onClick={async () => {
              if (confirm('Are you sure?')) {
                setCaption(null);
                await auth.axios({
                  url: `https://www.googleapis.com/youtube/v3/captions?id=${encodeURIComponent(captionId)}`,
                  method: 'DELETE'
                });
                delete cache.captions[videoId][captionId];
                if (Object.keys(cache.captions[videoId]).length === 0 && typeof cache.videos[videoId] === 'object') {
                  cache.videos[videoId].hasCaptions = false;
                  delete cache.captions[videoId];
                }
                route(`/videos/${videoId}/captions`, false);
              }
            }}>Delete</button>
            <button onClick={async () => {
              if (caption === false) throw new Error('This should be unreachable');

              if (confirm('Are you sure?')) {
                setCaption(null);
                const res = await auth.axios({
                  url: `https://www.googleapis.com/youtube/v3/captions?part=id,snippet`,
                  method: 'PUT',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  data: JSON.stringify({
                    id: captionId,
                    snippet: {
                      isDraft: !caption.isDraft
                    }
                  })
                });
                cache.captions[videoId][res.data.id] = res.data.snippet;
                setCaption(await getCaption(auth.axios, videoId, captionId));
                setTextareaContent(JSON.stringify(caption, undefined, 2));
              }
            }}>{caption === false || caption.isDraft ? 'Publish' : 'Unpublish'}</button>
          </>)
        }
      </div>
    </div>
  )
};

export default Caption;
