import { FunctionalComponent, h } from 'preact';

const NotFound: FunctionalComponent<unknown> = () => {
  return (
    <div className="c">
      <h2>Page not found</h2>
    </div>
  )
};

export default NotFound;
