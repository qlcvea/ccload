import { Component } from 'preact';
import { route } from 'preact-router';

export default class CaptionList extends Component<{ videoId: string }> {
  componentWillMount = () => {
    route(`/videos/${this.props.videoId}`, true);
  }

  render() {
    return null;
  }
}
