import { FunctionalComponent, h } from 'preact';
import { Link, route } from 'preact-router';
import { useContext, useState } from 'preact/hooks';
import { authContext } from '../components/auth';
import Loading from '../components/loading';
import { CachedCaption, CachedVideo, getCaptions, getVideo } from '../cache';

const Video: FunctionalComponent<{ videoId: string }> = ({ videoId }) => {
  const auth = useContext(authContext);
  const [ video, setVideo ] = useState<CachedVideo | undefined | null>(undefined);
  const [ captions, setCaptions ] = useState<{ [captionId: string]: CachedCaption } | undefined | null>(undefined);

  if (typeof video === 'undefined') {
    if (auth.signedIn) {
      setVideo(null);
      setCaptions(null);
      (async () => {
        const video = await getVideo(auth.axios, videoId);
        setVideo(video);
        if (video.hasCaptions) {
          setCaptions(null);
          setCaptions(await getCaptions(auth.axios, videoId));
        } else {
          setCaptions(undefined);
        }
      })();
    }
  }

  if (typeof video === 'undefined' || video === null)
    return (
      <Loading />
    );

  return (
    <div className="m">
      <div className="i">
        <img src={video.thumbnails.high.url} alt={video.thumbnails.high.url} />
        <div>
          <h2>{video.title}</h2>
          <p>{video.description}</p>
        </div>
      </div>
      {(() => {
        switch (captions) {
          case undefined:
            return (<p class="c">This video has no captions.</p>);
          case null:
            return (<p class="c">Loading caption list...</p>);
          default:
            return (
              <ul>
                {Object.keys(captions).map((captionId) => (
                  <li key={captionId}><Link href={`/videos/${videoId}/captions/${captionId}`}>{captions[captionId].language}{(typeof captions[captionId].name === 'string' && captions[captionId].name.length >= 1 ? ` - ${captions[captionId].name}` : null)}</Link></li>
                ))}
              </ul>
            );
        }
      })()}
      <div className="p">
        <button onClick={() => route(`/videos/${videoId}/captions/new`)}>Add captions</button>
      </div>
    </div>
  )
};

export default Video;
