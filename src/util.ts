// Generate a random integer r with equal chance in  min <= r < max.
// https://stackoverflow.com/a/41452318
function cryptoRandomNumber(min: number, max: number): number {
  const range = max - min;
  if (range <= 0) {
    throw new Error('max must be larger than min');
  }
  const requestBytes = Math.ceil(Math.log2(range) / 8);
  if (!requestBytes) { // No randomness required
    return min;
  }
  const maxNum = Math.pow(256, requestBytes);
  const ar = new Uint8Array(requestBytes);

  // eslint-disable-next-line no-constant-condition
  while (true) {
    window.crypto.getRandomValues(ar);

    let val = 0;
    for (let i = 0; i < requestBytes; i++) {
      val = (val << 8) + ar[i];
    }

    if (val < maxNum - maxNum % range) {
      return min + (val % range);
    }
  }
}

function cryptoRandomString(length: number, chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'): string {
  if (chars.length < 1) throw new Error('chars must contain at least 1 character');
  let result = '';
  for (let i = 0; i < length; i++) {
    result += chars[cryptoRandomNumber(0, chars.length)];
  }
  return result;
}

export { cryptoRandomNumber, cryptoRandomString }