import { h } from 'preact';
import { Route, Router } from 'preact-router'; // TODO
import { authContext, AutomatedAuthProvider, beginSignIn } from './auth';
import values from '../../values.json';
import BrowserImplicitAuthHelper from '../auth-helpers/browser-implicit-auth-helper';
import Header from './header';
import Home from '../routes/home';
import VideoList from '../routes/video-list';
import Video from '../routes/video';
import CaptionList from '../routes/caption-list';
import Caption from '../routes/caption';

const authHelper = new BrowserImplicitAuthHelper(values.clientId);

const App = () => (
  <AutomatedAuthProvider authHelper={authHelper}>
    <authContext.Consumer>
      {(auth) => {
        switch (auth.signedIn) {
          case true:
            return (
              <div>
                <Header />
                <Router>
                  <Route path="/" component={Home} />
                  <Route path="/videos" component={VideoList} />
                  <Route path="/videos/:videoId" component={Video} />
                  <Route path="/videos/:videoId/captions" component={CaptionList} />
                  <Route path="/videos/:videoId/captions/new" component={Caption} />
                  <Route path="/videos/:videoId/captions/:captionId" component={Caption} />
                </Router>
              </div>
            );
          case false:
            return (
              <div className="c">
                <p><a href="" onClick={(e) => {
                  e.preventDefault();
                  beginSignIn();
                }}>Sign in</a></p>
              </div>
            );
          default:
            return (
              <div className="c">
                <p>Please wait</p>
              </div>
            );
        }
      }}
    </authContext.Consumer>
  </AutomatedAuthProvider>
);

export default App;
