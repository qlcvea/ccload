import { FunctionalComponent, h } from 'preact';
import { useContext } from 'preact/hooks';
import { authContext, signOut } from '../components/auth';

const Header: FunctionalComponent<unknown> = () => {
  return (
    <div className="h">
      <h1>ccload</h1>
      <a href="" onClick={(e) => {
        e.preventDefault();
        signOut();
      }}>Sign out</a>
    </div>
  )
};

export default Header;
