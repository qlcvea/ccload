import { FunctionalComponent, h } from 'preact';

/**
 * A simple component to be used when a page is loading its content.
 */
const Loading: FunctionalComponent<unknown> = () => {
  return (
    <div className="c">
      <p>Loading</p>
    </div>
  )
};

export default Loading;
