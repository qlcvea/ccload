import axios, { AxiosInstance } from 'axios';
import { createContext, FunctionalComponent, h } from 'preact';
import { useState } from 'preact/hooks';
import AuthHelper from '../auth-helpers/auth-helper';

type AuthenticationStateUnknown = {
  signedIn: null
};

type AuthenticationStateUnauthenticated = {
  signedIn: false
};

type AuthenticationStateAuthenticated = {
  signedIn: true,
  token: string,
  tokenExpires: Date
};

type AuthenticationStateKnown = AuthenticationStateUnauthenticated | AuthenticationStateAuthenticated;

type AuthenticationState = AuthenticationStateUnknown | AuthenticationStateKnown;

type AuthenticationContextValue = AuthenticationState & { axios: AxiosInstance };

const authContext = createContext<AuthenticationContextValue>({ signedIn: null, axios });

let beginSignIn: () => void = () => { throw new Error('Not ready'); };

let signOut: () => Promise<void> = () => { throw new Error('Not ready'); };

const AutomatedAuthProvider: FunctionalComponent<{ authHelper: AuthHelper }> = ({ authHelper, children }) => {
  const [ getAuthPromise, setGetAuthPromise ] = useState<Promise<unknown> | null>(null);
  const [ ctxValue, setCtxValue ] = useState<AuthenticationContextValue>({ signedIn: null, axios });

  if (getAuthPromise === null) {
    setGetAuthPromise(authHelper.getAuth().then((auth) => {
      setCtxValue({
        axios: auth.signedIn ? axios.create({
          headers: {
            Authorization: `Bearer ${auth.token}`
          }
        }) : axios,
        ...auth
      });
    }));
  }

  beginSignIn = authHelper.beginSignIn;
  signOut = async () => { setCtxValue({ signedIn: null, axios }); await authHelper.signOut(); setCtxValue({ signedIn: false, axios }); }

  return (
    <authContext.Provider value={ctxValue}>{children}</authContext.Provider>
  );
};

export { authContext, AutomatedAuthProvider, beginSignIn, signOut };
export type { AuthenticationStateUnknown, AuthenticationStateUnauthenticated, AuthenticationStateAuthenticated, AuthenticationStateKnown, AuthenticationState };
