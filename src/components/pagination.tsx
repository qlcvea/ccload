import { FunctionalComponent, h } from 'preact';

const Header: FunctionalComponent<{ onNext: (() => void) | null, onPrevious: (() => void) | null, }> = ({ onNext, onPrevious }) => {
  return (
    <div className="p">
      <button disabled={onPrevious === null} onClick={onPrevious === null ? () => null : onPrevious}>Previous</button>
      <button disabled={onNext === null} onClick={onNext === null ? () => null : onNext}>Next</button>
    </div>
  )
};

export default Header;
